#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 18:41:47 2024

@author: theo
"""

#todo:
#refine RYM artist hunting
#switch rym scraper from selenium to request unless there's a reason for selenium (probably helps avoid getting banned)
#read and append to old databases
#strip everything after ? in the urls
#add day of the week
#make data floats, ints
#should probably use a pandas multiindex

#model is to have a big database dataframe that contains all the info. Just tons of info
#when exporting it to a readable csv, that's when I can filter it

import requests
import json
import pandas as pd
from bs4 import BeautifulSoup
import csv
import datetime
import numpy as np
import argparse

import rymscraper

from warnings import simplefilter
simplefilter(action="ignore", category=pd.errors.PerformanceWarning) #shut pandas up about how I add columns
pd.options.mode.chained_assignment = None #and shut it up about some other thing

network = rymscraper.RymNetwork()



def main(start_date, end_date, outfile = 'concerts.csv'):
    url = generate_url(start_date, end_date)
    html = requests.get(url)
    soup = BeautifulSoup(html.content, "html.parser")
    
    max_pages = get_page_count(soup)
    df = pd.DataFrame({})
    for page_num in range(max_pages):
        print('Page ' + str(page_num + 1) + '/' + str(max_pages) + ' in the concert list')
        url = generate_url(start_date, end_date, page = page_num + 1)
        page = requests.get(url)
        soup = BeautifulSoup(page.content, "html.parser")        
        page_df = soup_to_df(soup)
        df = pd.concat([df, page_df], axis = 0, ignore_index=True)
        
    df.to_pickle('db.pkl')
    trim_df = trim_function(df)
    output_csv(trim_df, outfile)
    print('Finished writing ' + str(outfile))

#valid date formats:
#mm/dd/yyyy
#mm/dd/yy
#mm/dd
def generate_url(start_date, end_date, page = 1):
    
    minday = start_date.split('/')[1]
    minmonth = start_date.split('/')[0]
    if start_date.count('/') == 2:
        minyear = start_date.split('/')[2]
        if len(minyear) == 2:
            minyear = str(int(minyear) + 2000) #will need to change this in about 76 years
    elif start_date.count('/') == 1:
        minyear = datetime.datetime.now().date().strftime("%Y")
    else:
        raise ValueError("Start date is somehow formatted wrong. Entered " + start_date)
        
    maxday = end_date.split('/')[1]
    maxmonth = end_date.split('/')[0]
    if end_date.count('/') == 2:
        maxyear = end_date.split('/')[2]
        if len(maxyear) == 2:
            maxyear = str(int(maxyear) + 2000) #will need to change this in about 76 years
    elif end_date.count('/') == 1:
        maxyear = datetime.datetime.now().date().strftime("%Y")
    else:
        raise ValueError("End date is somehow formatted wrong. Entered " + end_date)
    
    mindate = str(minmonth) + "%2F" + str(minday) + "%2F" + str(minyear)
    maxdate = str(maxmonth) + "%2F" + str(maxday) + "%2F" + str(maxyear)
    
    url = "https://www.songkick.com/metro-areas/9179-us-austin?filters%5BmaxDate%5D=" + maxdate + "&filters%5BminDate%5D=" + mindate + "&page=" + str(page) + "#metro-area-calendar"
    return url

def get_page_count(soup):
    paginations = soup.find_all(class_='pagination')
    if len(paginations) != 0:
        pagination = paginations[0]
        final_page_link = list(pagination.children)[-3]
        final_page_num_str = final_page_link['aria-label'].split(' ')[1]
        final_page = int(final_page_num_str)
        return final_page
    else:
        return 1 #first page will be enough

def soup_to_df(soup):
    # page = requests.get(url)
    # soup = BeautifulSoup(page.content, "html.parser")

    listings = soup.find_all(class_="event-listings-element")

    df = pd.DataFrame({})
    
    #columns from rym
    artist_columns = ['Name', 'Genres', 'Formed', 'Disbanded', 'Members', 'Related Artists', 'Also Known As', 'Notes']

    for listing in listings:
        mfmt = listing.find_all(class_="microformat")
        script = mfmt[0].find('script')
        sctext = script.text[1:-1]
        scdict = json.loads(sctext)
        unlisted_dict = unlist_nested(scdict)
        # unlisted_json = json.dumps(unlisted_dict)
        flat_df = pd.json_normalize(unlisted_dict)
        
        #need to more intelligently add artist info
        #the problem is when an event has the most performers, and one of their high numbered ones can't be found
        #I think I need a minimum of columns that I'll always have. RYMname = nan and releases = 0 should do
        performer_index = 0
        while ('performer.' + str(performer_index) + '.name') in flat_df.columns:
            artist = flat_df['performer.' + str(performer_index) + '.name'][0]
            print('Performer ' + str(performer_index) + ' in event "' + flat_df['name'][0] + '"')
            artist_infos, discog = network.get_artist_infos(name=artist, sleep = 2)
            
            for col in artist_columns:
                if col in artist_infos.keys():
                    flat_df['performer.' + str(performer_index) + '.' + col] = artist_infos[col]
                else:
                    flat_df['performer.' + str(performer_index) + '.' + col] = float('nan')
            flat_df = flat_df.rename(columns={'performer.' + str(performer_index) + '.Name' : 'performer.' + str(performer_index) + '.RYMname'})

            
            #if there are any releases
            discog_df = pd.DataFrame(discog)
            discog_size = len(discog_df)
            if(discog_size != 0):
                for index in range(discog_size):
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.name'] = discog_df['Name'][index]
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.category'] = discog_df['Category'][index]
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.url'] = discog_df['URL'][index] #not crucial
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.date'] = discog_df['Date'][index] #not crucial
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.year'] = discog_df['Year'][index]
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.rating'] = discog_df['Average Rating'][index]
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.ratings'] = discog_df['Ratings'][index]        
                    flat_df['performer.' + str(performer_index) + '.release.' + str(index) + '.reviews'] = discog_df['Reviews'][index] #not crucial
                    # print(str(index) + ' of ' + str(discog_size) + ' in discog, performer ' + str(performer_index) + ' in event "' + flat_df['name'][0] + '"')
                
                flat_df['performer.' + str(performer_index) + '.releases'] = int(discog_size)
            else: #if there aren't any, 0 releases
                flat_df['performer.' + str(performer_index) + '.releases'] = int(discog_size)
                
                

            performer_index = performer_index + 1
        
        flat_df['performers'] = int(performer_index)
        df = pd.concat([df, flat_df], axis = 0, ignore_index=True)
        #should also remove tracking info from links here
        #also should save the database here in case we get banned
    return df

#necessary for turning nested dictionary list structure into nested dicts only
def unlist_nested(in_dict):
    for key in in_dict.keys():
        if type(in_dict[key]) == list:
            list_len = len(in_dict[key])
            keys = list(range(list_len))
            
            in_dict[key] = {keys[i] : in_dict[key][i] for i in range(list_len)}
        elif type(in_dict[key]) == dict:
            in_dict[key] = unlist_nested(in_dict[key])
    return in_dict


def reindex_releases(dataframe):
    user_dataframe = dataframe.copy()
    #assumes every release has the same attributes
    release_cols = []
    for df_column in user_dataframe.columns:
        if df_column.startswith('performer.0.release.0.'):
            release_cols = release_cols + [df_column.removeprefix('performer.0.release.0.')]
    
    for event_index in range(len(dataframe)):
        for performer_index in range(user_dataframe['performers'][event_index]):
            release_num = 0
            empties = []
            counted_releases = 0 #stop moving things once I've acounted for every release
            while(counted_releases < user_dataframe['performer.' + str(performer_index) + '.releases'][event_index]):
                #if I find an empty release
                if(pd.isna(user_dataframe['performer.' + str(performer_index) + '.release.' + str(release_num) + '.' + release_cols[0]][event_index])):
                    empties = empties + [release_num] #add it to the list of empties
                    # print('release ' + str(release_num) + ' is empty, adding it to list: ' + str(empties))
                else: #if it's not empty
                    counted_releases = counted_releases + 1
                    # print('release ' + str(release_num) + ' is not empty, counted ' + str(counted_releases))
                    if len(empties) > 0: #figure out whether it needs to move
                        for col in release_cols: #move every release tag over
                            temp = user_dataframe['performer.' + str(performer_index) + '.release.' + str(release_num) + '.' + col][event_index]
                            user_dataframe['performer.' + str(performer_index) + '.release.' + str(empties[0]) + '.' + col][event_index] = temp
                            user_dataframe['performer.' + str(performer_index) + '.release.' + str(release_num) + '.' + col][event_index] = float('nan')
                        empties = empties + [release_num] #we just vacated a cell. add it
                        empties = empties[1:] #the empty we filled needs to leave the list
                release_num = release_num + 1
                
    return user_dataframe


#look for empty releases, and then delete them. NOT column by column
def cleanup_empty_release_cols(dataframe):
    copy_df = dataframe.copy()
    release_cols = []
    for df_column in copy_df.columns:
        if df_column.startswith('performer.0.release.0.'):
            release_cols = release_cols + [df_column.removeprefix('performer.0.release.0.')]

    drop_cols = []
    for col in copy_df.columns:
        #if we're looking at a release.x.name column:
        if 'release' in col and col.endswith('name'):
            #if every cell in that column is empty:
            if pd.isna(copy_df[col]).all():
                #then drop all the columns associated with that release
                for rel_col in release_cols:
                    drop_cols = drop_cols + [(col.removesuffix('name') + rel_col)]
    copy_df = copy_df.drop(columns = drop_cols)
    return copy_df            


def cleanup_empty_cols(dataframe):
    user_dataframe = dataframe.copy()
    drop_cols = []
    for col in dataframe.columns:
        if pd.isna(user_dataframe[col]).all():
            drop_cols = drop_cols + [col]
            
    user_dataframe = user_dataframe.drop(columns = drop_cols)
    return user_dataframe

#pare the dataframe down to a dataframe of things I care about
#this varies heavily from user to user
#here I'm using it to delete a bunch of extraneous columns
#remove every release that isn't an EP or album
#assign a cscore (concert score) to each release
#give every artist a cscore that's the max of their releases
#remove all but their best release
def trim_function(dataframe): 
    user_dataframe = dataframe.copy()
    min_ratings = 3
    min_cscore = 0
    
    #event: start date, url, link, performers
    #performer: name, rymname, genres, releases 
    #release: name, category, year, rating, ratings, cscore
    
    event_categories = ['startDate', 'location.name', 'url', 'performers'] #things I care about for each event
    performer_categories = ['name', 'RYMname', 'releases'] #things I care about for each performer
    release_categories = ['name', 'category', 'year', 'rating', 'ratings', 'cscore'] #things I care about for each release
    
    
    all_columns = dataframe.columns
    for df_column in all_columns:
        if not(df_column in event_categories or df_column.startswith('performer.')):
            user_dataframe = user_dataframe.drop(columns = df_column)
        
    for df_column in user_dataframe.columns:    
        if df_column.endswith('.url'):
            user_dataframe = user_dataframe.drop(columns = df_column)
        if df_column.endswith('.date'):
            user_dataframe = user_dataframe.drop(columns = df_column)
        if df_column.endswith('.reviews'):
            user_dataframe = user_dataframe.drop(columns = df_column)
        
    
    #assumes that every release has the same columns, and every artist has the same columns
    release_cols = []
    for df_column in user_dataframe.columns:
        if df_column.startswith('performer.0.release.0.'):
            release_cols = release_cols + [df_column.removeprefix('performer.0.release.0.')]
            
    performer_cols = []
    for df_column in user_dataframe.columns:
        if df_column.startswith('performer.0.') and not df_column.startswith('performer.0.release.'):
            performer_cols = performer_cols + [df_column.removeprefix('performer.0.')]
    
    
    #for now, just remove non-albums and EPs, and update the releases number
    for event in range(len(user_dataframe)):
        for performer in range(user_dataframe['performers'][event]):
            for release in range(int(user_dataframe['performer.' + str(performer) + '.releases'][event])):
                if user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.category'][event] not in ['Album', 'EP']:
                    for release_suffix in release_cols:
                        user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.' + release_suffix][event] = float('nan')
                    user_dataframe['performer.' + str(performer) + '.releases'][event] -= 1 #modify releases to only count EPs and albums
        #     if user_dataframe['performer.' + str(performer) + '.releases'][event] == 0: #if we killed every release
        #         for performer_suffix in performer_cols: #remove the performer
        #             user_dataframe['performer.' + str(performer) + '.' + performer_suffix][event] = float('nan')
        #         user_dataframe['performers'][event] -= 1 #fewer performers at the event
        # if user_dataframe['performers'][event] == 0: #if we killed every performer at the event
        #     user_dataframe = user_dataframe.drop(rows = event)
    
    
    #fill in the empty slots, delete unneeded columns
    user_dataframe = reindex_releases(user_dataframe)
    user_dataframe = cleanup_empty_release_cols(user_dataframe)
    
    
    #I'd go see a band that made a >3.9 in >2000, or a >3.7 in >2010, etc.
    cscore_years = np.array([2000, 2010, 2015, 2020, 2022])
    cscore_ratings = np.array([3.9, 3.7, 3.6, 3.5, 3.4])
    def cscore_subtractor(year):
        return np.interp(year, cscore_years, cscore_ratings)
    
    #calculate cscore for each remaining release
    #this is a very stupid way of adding the column, but it's hard to do in one loop
    #what I'd want is to specify one cell and let the rest be nan if its the first use of that column
    #but I don't think that's behavior pandas is ok with
    for event in range(len(user_dataframe)):
        for performer in range(user_dataframe['performers'][event]):
            for release in range(int(user_dataframe['performer.' + str(performer) + '.releases'][event])):
                release_string = 'performer.' + str(performer) + '.release.' + str(release) + '.'
                user_dataframe[release_string + 'cscore'] = float('nan')
    
    for event in range(len(user_dataframe)):
        for performer in range(user_dataframe['performers'][event]):
            for release in range(int(user_dataframe['performer.' + str(performer) + '.releases'][event])):
                release_string = 'performer.' + str(performer) + '.release.' + str(release) + '.'
                if user_dataframe[release_string + 'rating'][event] != '' and user_dataframe[release_string + 'year'][event] != '':
                    user_dataframe[release_string + 'cscore'][event] = float(user_dataframe[release_string + 'rating'][event]) - float(cscore_subtractor(user_dataframe[release_string + 'year'][event]))
                else:
                    user_dataframe[release_string + 'cscore'][event] = float('nan')
    release_cols = release_cols + ['cscore']
    
    
    #for each performer, delete all but their best releases
    #"best" also makes sure that they have enough ratings
    performer_max_cscore_release = 0
    for event in range(len(user_dataframe)):
        for performer in range(user_dataframe['performers'][event]):
            best_cscore = -999 #dummy value
            performer_max_cscore_release = 0
            #figure out which release is best. for each release:
            for release in range(int(user_dataframe['performer.' + str(performer) + '.releases'][event])):
                #best_cscore = user_dataframe['performer.' + str(performer) + '.release.' + str(performer_max_cscore_release) + '.cscore'][event]
                release_string = 'performer.' + str(performer) + '.release.' + str(release) + '.'
                #if the current release's cscore is higher than the best:
                if user_dataframe[release_string + 'cscore'][event] > best_cscore and float(user_dataframe[release_string + 'ratings'][event]) >= min_ratings:
                    #new best cscore, and new release for that cscore
                    performer_max_cscore_release = release
                    best_cscore = user_dataframe[release_string + 'cscore'][event]
            
            starting_releases = int(user_dataframe['performer.' + str(performer) + '.releases'][event])
            for release in range(starting_releases):
                #if we aren't looking at their best release, delete it
                release_string = 'performer.' + str(performer) + '.release.' + str(release) + '.'
                if release != performer_max_cscore_release:
                    for col in release_cols:
                        # print('deleting ' + str(release_string) + col)
                        user_dataframe[release_string + col][event] = float('nan')
                    user_dataframe['performer.' + str(performer) + '.releases'][event] -= 1
                
            
    user_dataframe = reindex_releases(user_dataframe)
    user_dataframe = cleanup_empty_release_cols(user_dataframe)
    
    #event: start date, url, link, performers
    #performer: name, rymname, genres, releases 
    #release: name, category, year, rating, ratings, cscore
    
    
    #drop all but the desired categories
    event_categories = ['startDate', 'location.name', 'url', 'performers'] #things I care about for each event
    performer_categories = ['name', 'RYMname', 'Genres', 'releases'] #things I care about for each performer
    release_categories = ['name', 'category', 'year', 'rating', 'ratings', 'cscore'] #things I care about for each release
    
    drop_columns = []
    
    for column in user_dataframe.columns:
        split_column = column.split('.')
        if len(split_column) < 3:
            if not column in event_categories:
                drop_columns = drop_columns + [column]
        if len(split_column) == 3:
            if not split_column[2] in performer_categories:
                drop_columns = drop_columns + [column]
        if len(split_column) == 5:
            if not split_column[4] in release_categories:
                drop_columns = drop_columns + [column]
            
    user_dataframe = user_dataframe.drop(columns = drop_columns)
    
    #reorder columns
    #reorders event categories, leaves everything else
    user_dataframe = user_dataframe[event_categories + [col for col in user_dataframe if col not in event_categories]]
    
    for performer in range(np.max(user_dataframe['performers'])):
        desired_order = []
        for performer_col in performer_categories:
            #desired order is the order I'll have to look at the columns to get the order I want
            desired_order = desired_order + [user_dataframe.columns.get_loc('performer.' + str(performer) + '.' + performer_col)]
    
        sorted_order = desired_order.copy()
        sorted_order.sort()
        user_dataframe.iloc[:,sorted_order] = user_dataframe.iloc[:, desired_order]
        
    for performer in range(np.max(user_dataframe['performers'])):
        max_releases = np.max(user_dataframe['performer.' + str(performer) + '.releases'])
        for release in range(int(max_releases)):
            desired_order = []
            for release_col in release_categories:
                #desired order is the order I'll have to look at the columns to get the order I want
                desired_order = desired_order + [user_dataframe.columns.get_loc('performer.' + str(performer) + '.release.' + str(release) + '.' + release_col)]
        
            sorted_order = desired_order.copy()
            sorted_order.sort()
            user_dataframe.iloc[:,sorted_order] = user_dataframe.iloc[:, desired_order]
            
            
    # remove altogether low cscore releases, and those with < 3 ratings
    valid_releases = 0
    for event in range(len(user_dataframe)):
        valid_releases = 0
        for performer in range(user_dataframe['performers'][event]):
            for release in range(int(user_dataframe['performer.' + str(performer) + '.releases'][event])):
                #handle an empty rating
                if user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.ratings'][event] == '':
                    for release_suffix in release_cols:
                        user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.' + release_suffix][event] = float('nan')
                    user_dataframe['performer.' + str(performer) + '.releases'][event] -= 1 #modify releases
                #if the rating isn't empty, can actually assess the value
                elif user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.cscore'][event] < 0 \
                    or float(user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.ratings'][event]) <= min_ratings:
                        for release_suffix in release_cols:
                            user_dataframe['performer.' + str(performer) + '.release.' + str(release) + '.' + release_suffix][event] = float('nan')
                        user_dataframe['performer.' + str(performer) + '.releases'][event] -= 1 #modify releases
            #sum the valid releases for every artist at the show
            valid_releases = valid_releases + user_dataframe['performer.' + str(performer) + '.releases'][event]
        #if the event has no valid releases left, delete it
        if valid_releases == 0:
            user_dataframe = user_dataframe.drop([event])
    #reindex
    user_dataframe = user_dataframe.reset_index(drop = True)
    #fill in the empty slots, delete unneeded columns
    user_dataframe = reindex_releases(user_dataframe)
    user_dataframe = cleanup_empty_release_cols(user_dataframe)
    
    return user_dataframe
            
#removes duplicates from a list while keeping it in order
def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]
    
def output_csv(dataframe, filename = 'concerts.csv'): #output the csv to something reasonably readable
    
    #something starting with performer.x.release.y prints on every line
    #something starting with performer.x. not release prints on every new performer
    #something starting with anything else prints on every new event
    #modify the dataframe columns to work with that scheme

    with open(filename, 'w', newline='', encoding = 'UTF-8') as csvfile:
        csvwriter = csv.writer(csvfile, dialect='excel')
        
        columns = dataframe.columns
        event_columns = []
        performer_columns = []
        release_columns = []
        for column in columns:
            split_column = column.split('.')
            if len(split_column) < 3:
                event_columns = event_columns + [column]
            if len(split_column) == 3:
                performer_columns = performer_columns + [split_column[2]]
            if len(split_column) == 5:
                release_columns = release_columns + [split_column[4]]
            
        #remove duplicates
        event_columns = f7(event_columns)
        performer_columns = f7(performer_columns)
        release_columns = f7(release_columns)
        
        total_categories = len(event_columns) + len(performer_columns) + len(release_columns)
        line = event_columns + performer_columns + release_columns
        csvwriter.writerow(line)
        
        line = ['']*total_categories
        for event_num in range(len(dataframe)):
            #on new event, set event columns
            for index in range(len(event_columns)):
                line[index] = dataframe[event_columns[index]][event_num]
            
            for performer_index in range(dataframe['performers'][event_num]):
                #on new performer, set performer categories
                for index in range(len(performer_columns)):
                    line[index + len(event_columns)] = dataframe['performer.' + str(performer_index) + '.' + performer_columns[index]][event_num]
                
                
                #if 0 releases, leave those columns blank
                if int(dataframe['performer.' + str(performer_index) + '.releases'][event_num]) == 0:
                   csvwriter.writerow(line)
                   line = ['']*total_categories #reset the line after each event to clear performer and event columns
                else:
                   for release_index in range(int(dataframe['performer.' + str(performer_index) + '.releases'][event_num])):
                       for index in range(len(release_columns)):
                           line[index + len(event_columns) + len(performer_columns)] = dataframe['performer.' + str(performer_index) + '.release.' + str(release_index) + '.' + release_columns[index]][event_num]
                       csvwriter.writerow(line)
                       line = ['']*total_categories #reset the line after each event to clear performer and event columns

if __name__ == '__main__':
    #apparently argumentparser is best practice. seems like a big pain in the ass to me
    #had to jump through hoops so I can still run the script in Spyder and not get an error
    parser = argparse.ArgumentParser(description = "Concert Finder")
    parser.add_argument("start_date", nargs='?', help="Look for concerts in a timespan starting here. Format as e.g. '3/25' for March 25")
    parser.add_argument("end_date", nargs='?', help="Look for concerts in a timespan ending here. Format as e.g. '3/28' for March 28")
    args = parser.parse_args()
    start_date = args.start_date
    end_date = args.end_date
    
    if start_date != None and end_date != None:
        main(start_date, end_date)
    else:
        print("Didn't get a start and end date. Not running")
    
    
# main('4/18', '4/22')
#only managed to get to 4/14 before getting banned
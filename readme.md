# Concert Finder

## What is it?
It's a python tool to look for concerts in a certain city over a certain range of dates, pull info about each concert, then look for info about each performer and their releases. The goal is to make it easier to figure out what shows are worth going to.

## How do I use it?
I'm assuming you have python installed and aren't missing any of the libraries this needs. If that's not the case, figure out from the error message what you're missing and install it.
- Open a command prompt and run the script with dates you want, e.g. `python concert_finder.py '4/26' '4/28'` to see concerts happening on April 26th, 27th, and 28th
- open an IDE like Spyder, run concert_finder.py, and call main() with a start and end date of interest

The output will be `df.pkl`, the database with all the info the program could find, and `concerts.csv`, a comma-separated value file with the info specified in `trim_function()`. The latter is the file you should be looking at when planning what shows to go to. You should be able to open it in Excel and have it be formatted reasonably well. I did all my testing with LibreOffice Calc, though. Edit `trim_function()` to change how the shows dataframe is processed, like to be less selective about how well-reviewed a band has to be to not get filtered, not tell you about Wednesday shows, only show post-rock, whatever.

*If you're interested in a city other than Austin, TX* you'll have to edit the `generate_url()` function.

## How does it work?
There are three major components:
- Pulling info on concerts
- Getting info on performers
- Filtering based on that

Concert info is taken from a website that aggregates concerts. You can choose the range of dates over which you want to look at concerts. For each concert, the performers are extracted so that the program can find info on them.

The performer info comes from a music review site. I'm using a fork of a GitHub repository by a dbeley. Dbeley mentions that the review site may ban your computer if you pull too much data in a short period, so I built a delay in between actions. I'd advise not turning it down, and using caution before pulling more than a few days of concerts at once.

The concert and performer info goes into a big dataframe, which is also saved to disk. From there, the user can do whatever they want to remove entries, pare down the columns to relevant info, and calculate what concerts they care about. The filtered dataframe is exported to a CSV file that should make the remaining data easy to read.

## Eventual changes
Good odds I don't do any of these since what I have is working. But it'd be nice to have
- day of the week (Friday, Saturday)
- better searching for performers. Current search is pretty stupid
- move away from selenium to pull the HTML, but that probably gets us banned faster
- remove tracking info from the URLs
- read `df.pkl` databases left over by previous runs and append to them
- support arbitrary cities. Not really sure how this would work as Austin is associated with number 9179 on the concert aggregator, and I don't know what other cities would be
- make the dataframes more sane by using a pandas MultiIndex. This would make things a lot cleaner but would require writing a ton of stuff so I'll most likely never do it

## This is cumbersome and not best practice, and violates the TOS!
Don't care lol, it's a proof of concept
